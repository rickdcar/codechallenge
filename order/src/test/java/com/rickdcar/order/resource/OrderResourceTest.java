package com.rickdcar.order.resource;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rickdcar.order.model.OrderRequest;
import com.rickdcar.order.service.OrderService;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = OrderResource.class)
public class OrderResourceTest {
    
    @MockBean
    private OrderService orderService;

    @Autowired
    private MockMvc mockMvc;
  
    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void givenOrderRequest_WhenServiceThrowException_ThenBadRequest() throws Exception {

        OrderRequest orderRequest = createOrderRequest(1L, 2L);
        when(orderService.processOrder(any(OrderRequest.class))).thenThrow(Exception.class);

        mockMvc.perform(post("/v1/orders")
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(orderRequest))).
                andExpect(status().isBadRequest());

    }

    @Test
    public void givenOrderRequest_WhenValidRequest_ThenOK() throws Exception {

        OrderRequest orderRequest = createOrderRequest(1L, 2L);
        when(orderService.processOrder(any(OrderRequest.class))).thenReturn(100.00D);

        mockMvc.perform(post("/v1/orders")
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(orderRequest))).
                andExpect(status().isOk());

    }

    @Test
    public void givenOrderRequest_WhenValidRequest_ThenOkWithTotalInBody() throws Exception {

        OrderRequest orderRequest = createOrderRequest(1L, 2L);
        when(orderService.processOrder(any(OrderRequest.class))).thenReturn(100.00D);

        MvcResult result = mockMvc.perform(post("/v1/orders")
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(orderRequest))).
                andReturn();

        assertTrue(result.getResponse().getContentAsString().equals("100.0"));

    }

    @Test
    public void givenOrderRequest_WhenNoName_ThenBadRequest() throws Exception {

        OrderRequest orderRequest = createOrderRequest(1L, 2L);
        orderRequest.setName(null);

        mockMvc.perform(post("/v1/orders")
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(orderRequest))).
                andExpect(status().isBadRequest());

    }

    @Test
    public void givenOrderRequest_WhenNoSurname_ThenBadRequest() throws Exception {

        OrderRequest orderRequest = createOrderRequest(1L, 2L);
        orderRequest.setSurname(null);

        mockMvc.perform(post("/v1/orders")
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(orderRequest))).
                andExpect(status().isBadRequest());

    }


    @Test
    public void givenOrderRequest_WhenNoEmail_ThenBadRequest() throws Exception {

        OrderRequest orderRequest = createOrderRequest(1L, 2L);
        orderRequest.setEmail(null);

        mockMvc.perform(post("/v1/orders")
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(orderRequest))).
                andExpect(status().isBadRequest());

    }

    @Test
    public void givenOrderRequest_WhenEmailInvalid_ThenBadRequest() throws Exception {

        OrderRequest orderRequest = createOrderRequest(1L, 2L);
        orderRequest.setEmail("email");

        mockMvc.perform(post("/v1/orders")
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(orderRequest))).
                andExpect(status().isBadRequest());

    }    

    @Test
    public void givenOrderRequest_WhenNoPhones_ThenBadRequest() throws Exception {

        OrderRequest orderRequest = createOrderRequest(1L, 2L);
        orderRequest.setPhones(null);

        mockMvc.perform(post("/v1/orders")
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(orderRequest))).
                andExpect(status().isBadRequest());

    }

    private OrderRequest createOrderRequest(Long... ids) {

        OrderRequest orderRequest = new OrderRequest();
        orderRequest.setName("Nombre");
        orderRequest.setSurname("Apellidos");
        orderRequest.setEmail("email@email.com");
        orderRequest.setPhones(Arrays.asList(ids));

        return orderRequest;
    }

}
