package com.rickdcar.order.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import com.rickdcar.order.model.OrderRequest;
import com.rickdcar.order.model.Phone;
import com.rickdcar.order.repository.OrderRepository;
import com.rickdcar.order.service.impl.OrderServiceImpl;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class OrderServiceTest {


    private OrderService orderService;

    @MockBean
    private OrderRepository orderRepository;

    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();

    @Before
    public void setUp() throws Exception {
        orderService = new OrderServiceImpl(orderRepository);
    }

    @Test
    public void givenOrderRequest_WhenRepositoryThrowException_ThenThrowException() throws Exception {

        when(orderRepository.getPrices(anyList())).thenThrow(Exception.class);        
        exceptionRule.expect(Exception.class);
        orderService.processOrder(createOrderRequest(1L, 2L));
    }


    @Test
    public void givenOrderRequest_WhenRepositoryGivesPhones_ThenOk() throws Exception {
        
        List<Phone> catalogue = createListPhones(3);
        Double total = catalogue.stream()
                        .mapToDouble(p -> p.getPrize()).sum();

        when(orderRepository.getPrices(anyList())).thenReturn(catalogue);        

        Double suma = orderService.processOrder(createOrderRequest(1L, 2L));
        
        assertEquals(total, suma);
    }

    @Test
    public void givenOrderRequest_WhenRepositoryNotExistPhone_ThenThrowIllegalArgumentException() throws Exception {
        
        List<Phone> catalogue = createListPhones(2);
        
        exceptionRule.expect(IllegalArgumentException.class);               
        when(orderRepository.getPrices(anyList())).thenReturn(catalogue);        
        
        orderService.processOrder(createOrderRequest(1L, 2L));
    }
        

    private OrderRequest createOrderRequest(Long... ids) {

        OrderRequest orderRequest = new OrderRequest();
        orderRequest.setName("Nombre");
        orderRequest.setSurname("Apellidos");
        orderRequest.setEmail("email@email.com");
        orderRequest.setPhones(Arrays.asList(ids));

        return orderRequest;
    }

    private List<Phone> createListPhones(int number) {
        return IntStream.range(1, number)
                .mapToObj(i -> createPhone(i))
                .collect(Collectors.toList());
    }

    private Phone createPhone(int i) {
        Phone phone = new Phone();
        phone.setId(new Long(i));
        phone.setName("Nombre" + i);
        phone.setDescription("Descripcion" + i);
        phone.setPrize(Math.random() * 100.00D);
        phone.setImage("Imagen" + i);
        return phone;

    }    

}
