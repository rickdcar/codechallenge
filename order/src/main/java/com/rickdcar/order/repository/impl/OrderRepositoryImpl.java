package com.rickdcar.order.repository.impl;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rickdcar.order.model.Order;
import com.rickdcar.order.model.Phone;
import com.rickdcar.order.repository.OrderRepository;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import lombok.extern.java.Log;

@Repository
@Log
public class OrderRepositoryImpl implements OrderRepository {

    @Value("${order.catalogUrl}")
    private String catalogUrl;

    private RestTemplate restTemplate = new RestTemplate();

    @Override
    public List<Phone>  getPrices(List<Long> ids) throws Exception {
        log.info(">>>> getCatalog ");
        log.info("Host: " + catalogUrl);
        try {
            
            UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(catalogUrl)
                                .queryParam("ids", ids.stream().map(i -> "" + i).collect(Collectors.joining(",")));

            Phone[] response = restTemplate.getForObject(builder.toUriString(), Phone[].class);
            log.info("<<<< getCatalog ");
            return Arrays.asList(response);
        } catch (Exception ex) {
            log.severe(ex.getMessage());
             throw new Exception(ex);
        }
    }

    @Override
    public void save(Order order) throws Exception {
        log.info(">>>> save ");
        ObjectMapper mapper = new ObjectMapper();
         log.info(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(order));    
         log.info("<<<< save ");
        }


}
