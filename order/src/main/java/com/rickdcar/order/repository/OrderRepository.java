package com.rickdcar.order.repository;

import java.util.List;

import com.rickdcar.order.model.Order;
import com.rickdcar.order.model.Phone;

public interface OrderRepository {

    public List<Phone> getPrices(List<Long> ids) throws Exception;
    
    public void save(Order order) throws Exception;
}
