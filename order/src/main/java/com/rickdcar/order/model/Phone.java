package com.rickdcar.order.model;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Phone {

    private Long id;

    private String name;

    private String description;

    private Double prize;

    private String image;    
    
}
