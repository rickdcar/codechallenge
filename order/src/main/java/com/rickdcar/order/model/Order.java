package com.rickdcar.order.model;

import java.util.List;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Order {
    
    private String name;

    private String surname;

    private String email;

    private List<Phone> phones;

    private Double total;



}
