package com.rickdcar.order.service.impl;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import com.rickdcar.order.model.Order;
import com.rickdcar.order.model.OrderRequest;
import com.rickdcar.order.model.Phone;
import com.rickdcar.order.repository.OrderRepository;
import com.rickdcar.order.service.OrderService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.extern.java.Log;

@Service
@Log
public class OrderServiceImpl implements OrderService {

    private OrderRepository orderRepository;

    @Autowired
    public OrderServiceImpl(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    @Override
    public Double processOrder(OrderRequest orderRequest) throws Exception {

        log.info(">>>> getTotalOrder");
        List<Phone> phones = orderRepository.getPrices(orderRequest.getPhones());

        List<Long> idPhones = orderRequest.getPhones().stream().distinct().collect(Collectors.toList());

        Map<Long, Phone> orderPhones = phones.stream().filter(p -> idPhones.contains(p.getId()))
                .collect(Collectors.toMap(Phone::getId, Function.identity()));
        if (idPhones.size() != orderPhones.size()) {
            throw new IllegalArgumentException();
        }

        Double total = orderRequest.getPhones().stream().mapToDouble(p -> orderPhones.get(p).getPrize()).sum();
        Order order = mapOrder(orderRequest, orderPhones);
        order.setTotal(total);

        orderRepository.save(order);
        log.info("<<<< getTotalOrder");

        return total;
    }
    

    private Order mapOrder(OrderRequest orderRequest, Map<Long, Phone> catalog) {

        Order order = new Order();
        order.setName(orderRequest.getName());
        order.setSurname(orderRequest.getSurname());
        order.setEmail(orderRequest.getEmail());
        order.setPhones(orderRequest.getPhones().stream()
                            .map(p -> catalog.get(p))
                            .collect(Collectors.toList()));                            
        return order;
    }
}
