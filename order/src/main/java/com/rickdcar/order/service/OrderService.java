package com.rickdcar.order.service;

import com.rickdcar.order.model.OrderRequest;

public interface OrderService {
    
    public Double processOrder(OrderRequest order) throws Exception;
}
