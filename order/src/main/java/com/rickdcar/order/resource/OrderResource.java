package com.rickdcar.order.resource;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.rickdcar.order.model.OrderRequest;
import com.rickdcar.order.service.OrderService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/v1")
public class OrderResource {
    
    @Autowired
    private OrderService orderService;

    @PostMapping("orders")
    @ApiResponses(value = {@ApiResponse (code = 200, message = "OK"), @ApiResponse (code = 400, message = "BAD REQUEST")})
    public ResponseEntity<Double> calculateTotalPrize(@NotNull @Valid @RequestBody OrderRequest order) {

        try {
            return ResponseEntity.ok().body(orderService.processOrder(order));
        } catch (Exception ex) {
            return ResponseEntity.badRequest().build();
        }
    }
}
