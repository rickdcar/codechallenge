# Phone App

Este proyecto tiene dos microservicios: phone y order. Phone permite obtener un listado de todos los teléfonos de la base de datos o una lista de los mismos filtrada. Order acepta pedidos de teléfonos y devuelve el total del pedido si es aceptado.
Está implementado en Java8 y utiliza spring-boot para su desarrollo.

## Requisitos para construir el proyecto

- JDK 8 (o superior)

- Tener configurada la variable de entorno JAVA_HOME  

- Tener conexión a internet para la descarga de dependencias  


## Poner en marcha

### 1. Construir e instalar cada uno de los proyectos
Desde el directorio de cada proyecto este comando se bajará las dependencias que le falten, construirá el proyecto y pasará todos los test. En linux puede que haya que dar permisos de ejecución al fichero, 
Si ya se tiene maven instalado en la máquina puede utilizarse el comando **mvn** en vez de ***mvnw***  
Linux
```bash
./mvnw clean verify
```
Windows
```bash
mvnw clean verify
```

### 2. Ejecutar el proyecto

#### a. Ejecutarlo como una tarea maven

Linux
```bash
./mvnw spring-boot:run
```
Windows
```bash
mvnw spring-boot:run
```

#### b. Ejecutarlo desde el ejecutable creado

Desde la carpeta del proyecto acceder al directorio *target*

```bash
cd target
```

Ejecutar la fichero jar
```bash
java -jar order-1.0.0.jar
java -jar phone-1.0.0.jar
```
#### c. Ejecutarlo con contenedores docker

En la carpeta raiz hay un fichero [docker-compose.yaml](/docker-compose.yaml) para construir las imagenes de los proyectos.


El microservico phone se levantará escuchando peticiones por el puerto 8080 y el microservicio order lo hará por el puerto 8081. Los puertos pueden ser configurados en los ficheros de propiedades application.yaml mediante la propiedad ***server.port***  
Al levantarse el microservicio phone carga una serie de datos de teléfonos para poder empezar a trabajar. Estos datos se encuentran en el fichero [phones.json](/phone/src/main/resources/phones.json)

### 3. Probar

El proyecto lleva incorporadas las librerías de [swagger-ui](https://swagger.io/tools/swagger-ui/), por lo que una vez levantados se puede acceder a la definición del API a través de la direcciónes [http://localhost:8080/swagger-ui.html](http://localhost:8080/swagger-ui.html) y [http://localhost:8081/swagger-ui.html](http://localhost:8081/swagger-ui.html). Esta interfaz también permite la realización de pruebas.  


## API Phone

Una definición de la API en formato Swagger puede verse en los ficheros [api-phone-v1.json](order/src/main/resources/api-phone-v1.json)  
También puede consultarse cuando los microservicios están levantandos a través de [swagger-ui](http://localhost:8080/swagger-ui.html)


### 1. Consulta de catálogo

#### Método

GET

#### Path

/v1/phones/catalogues

#### Parámetros

No

#### Respuesta

Lista de todos los teléfonos. Cada teléfono tiene:

>  **id**  
>  Identificador del teléfono

>  **name**  
>  Nombre del teléfono

>  **description**  
>  Descripción del teléfono

>  **prize**  
>  Precio del teléfono

>  **image**  
>  Url de una imagen del teléfono  


#### Ejemplo
~~~
GET /v1/phones/catalogues
~~~
~~~
[
  {
    "id": 1,
    "name": "Phone 1",
    "description": "Description of Phone 1",
    "prize": 100.5,
    "image": "phone1.png"
  },
  {
    "id": 2,
    "name": "Phone 2",
    "description": "Description of Phone 2",
    "prize": 200.5,
    "image": "phone2.png"
  }
]
~~~

### 2. Consulta de teléfonos

#### Método

GET

#### Path

/v1/phones

#### Parámetros

>  **ids**  
>  Lista de identificadores del teléfonos separados por comas

#### Respuesta

Lista de todos los teléfonos que existen coincidentes con los pedidos. Cada teléfono tiene:

>  **id**  
>  Identificador del teléfono

>  **name**  
>  Nombre del teléfono

>  **description**  
>  Descripción del teléfono

>  **prize**  
>  Precio del teléfono

>  **image**  
>  Url de una imagen del teléfono  


#### Ejemplo
~~~
GET /v1/phones?ids=1,2
~~~
~~~
[
  {
    "id": 1,
    "name": "Phone 1",
    "description": "Description of Phone 1",
    "prize": 100.5,
    "image": "phone1.png"
  },
  {
    "id": 2,
    "name": "Phone 2",
    "description": "Description of Phone 2",
    "prize": 200.5,
    "image": "phone2.png"
  }
]
~~~



## API Order

Una definición de la API en formato Swagger puede verse en los ficheros [api-order-v1.json](order/src/main/resources/api-order-v1.json) json).
También puede consultarse cuando los microservicios están levantandos a través de [swagger-ui](http://localhost:8081/swagger-ui.html).


### 1. Procesado de orden

#### Método

POST

#### Path

/v1/orders

#### Parámetros

No

#### Payload

>  **name**  
>  Nombre del cliente

>  **surname**  
>  Apellidos del cliente

>  **email**  
>  Direcciópn de correo del cliente

>  **Phones**  
>  Lista de identificadores de teléfonos


#### Respuesta

>  Precio total de la orden


#### Ejemplo
~~~
POST /v1/orders
~~~
~~~
{
  "email": "string@string.com",
  "name": "string",
  "phones": [
    1,2
  ],
  "surname": "string"
}
~~~
~~~
301.5
~~~

