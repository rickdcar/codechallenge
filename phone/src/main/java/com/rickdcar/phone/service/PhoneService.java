package com.rickdcar.phone.service;

import java.util.List;

import com.rickdcar.phone.model.Phone;

public interface PhoneService {
    
    /**
     * Return catalogue of phones
     * @return
     */
    public List<Phone> findAll() throws Exception;

    /**
     * Return phones by criteria
     * @param ids
     * @return
     */
    public List<Phone> findByCriteria(List<Long> ids) throws Exception;
}
