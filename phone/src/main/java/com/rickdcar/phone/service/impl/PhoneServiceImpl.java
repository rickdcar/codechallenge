package com.rickdcar.phone.service.impl;

import java.util.List;

import com.rickdcar.phone.model.Phone;
import com.rickdcar.phone.repository.PhoneRepository;
import com.rickdcar.phone.service.PhoneService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PhoneServiceImpl implements PhoneService {

    private PhoneRepository phoneRepository;

    @Autowired
    public PhoneServiceImpl(PhoneRepository phoneRepository) {
        this.phoneRepository = phoneRepository;
    }

    @Override
    public List<Phone> findAll()  throws Exception {
        return phoneRepository.findAll();
    }

    @Override
    public List<Phone> findByCriteria(List<Long> ids)  throws Exception {
        return phoneRepository.findByCriteria(ids);
    }
    
}
