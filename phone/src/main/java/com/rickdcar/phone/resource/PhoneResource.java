package com.rickdcar.phone.resource;

import java.util.List;

import javax.validation.constraints.NotNull;

import com.rickdcar.phone.model.Phone;
import com.rickdcar.phone.service.PhoneService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/v1")
public class PhoneResource {
    
    @Autowired
    private PhoneService phoneService;

    @GetMapping("/phones/catalogues")
    @ApiResponses(value = {@ApiResponse (code = 200, message = "OK"), @ApiResponse (code = 400, message = "BAD REQUEST")})
    public ResponseEntity<Phone[]> getCatalog() {

        try {
            return ResponseEntity.ok().body(phoneService.findAll().toArray(new Phone[0]));
        } catch (Exception ex) {
            return ResponseEntity.badRequest().build();
        }
    }


    @GetMapping("/phones")
    @ApiResponses(value = {@ApiResponse (code = 200, message = "OK"), @ApiResponse (code = 400, message = "BAD REQUEST")})
    public ResponseEntity<Phone[]> getPhones(@NotNull @RequestParam List<Long> ids) {

        try {
            return ResponseEntity.ok().body(phoneService.findByCriteria(ids).toArray(new Phone[0]));
        } catch (Exception ex) {
            return ResponseEntity.badRequest().build();
        }
    }

}
