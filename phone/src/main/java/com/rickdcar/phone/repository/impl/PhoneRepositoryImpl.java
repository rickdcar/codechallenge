package com.rickdcar.phone.repository.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rickdcar.phone.model.Phone;
import com.rickdcar.phone.repository.PhoneRepository;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

@Repository
public class PhoneRepositoryImpl implements PhoneRepository {

    @Value("${phone.filedatabase}")
    private String fileDatabase;


    private List<Phone> database;


    /**
     * Return all phones in database
     */
    @Override
    public List<Phone> findAll() {
        return database;
    }

    /**
     * Return phones by criteria
     */
    @Override
    public List<Phone> findByCriteria(List<Long> idPhones) {

        if (idPhones == null) {
            return new ArrayList<Phone>();
        }
        List<Phone> result = database.stream()
                                .filter(p -> idPhones.contains(p.getId()))
                                .collect(Collectors.toList());
        return result;
    }

    /*
     *  Load phones from file
     */
    @PostConstruct
    private void loadPhones() throws JsonParseException, JsonMappingException, IOException {

        ObjectMapper mapper = new ObjectMapper();
        Phone[] phones = mapper.readValue(PhoneRepositoryImpl.class.getResourceAsStream("/" + fileDatabase), Phone[].class);
        database = Arrays.asList(phones);
    }


}


