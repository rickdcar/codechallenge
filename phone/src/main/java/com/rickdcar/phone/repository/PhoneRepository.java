package com.rickdcar.phone.repository;

import java.util.List;

import com.rickdcar.phone.model.Phone;

/**
 * 
 */
public interface PhoneRepository {
    
    public List<Phone> findAll();

    public List<Phone> findByCriteria(List<Long> idPhones);

}
