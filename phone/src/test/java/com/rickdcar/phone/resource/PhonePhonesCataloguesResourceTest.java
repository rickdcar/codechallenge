package com.rickdcar.phone.resource;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rickdcar.phone.model.Phone;
import com.rickdcar.phone.service.PhoneService;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = PhoneResource.class)
public class PhonePhonesCataloguesResourceTest {
    
    @MockBean
    private PhoneService phoneService;

    @Autowired
    private MockMvc mockMvc;
  
    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void givenPhonesRequest_WhenServiceThrowException_ThenBadRequest() throws Exception {

        when(phoneService.findAll()).thenThrow(Exception.class);

        mockMvc.perform(get("/v1/phones/catalogues"))
                .andExpect(status().isBadRequest());

    }

    @Test
    public void givenPhonesRequest_WhenServiceOk_ThenOk() throws Exception {

        when(phoneService.findAll()).thenReturn(new ArrayList<Phone>());

        mockMvc.perform(get("/v1/phones/catalogues"))
                .andExpect(status().isOk());

    }

    @Test
    public void givenPhonesRequest_WhenServiceOk_ThenOkBodyWithList() throws Exception {

        when(phoneService.findAll()).thenReturn(createListPhones(3));

        MvcResult result =  mockMvc.perform(get("/v1/phones/catalogues"))
                .andReturn();

        Phone[] phones =  objectMapper.readValue(result.getResponse().getContentAsString(), Phone[].class);
        assertAll(
            () -> assertEquals(result.getResponse().getStatus(), 200),
            () -> assertEquals(phones.length, 2)
        );

    }

    private List<Phone> createListPhones(int number) {
        return IntStream.range(1, number)
                .mapToObj(i -> createPhone(i))
                .collect(Collectors.toList());
    }

    private Phone createPhone(int i) {
        Phone phone = new Phone();
        phone.setId(new Long(i));
        phone.setName("Nombre" + i);
        phone.setDescription("Descripcion" + i);
        phone.setPrize(Math.random() * 100.00D);
        phone.setImage("Imagen" + i);
        return phone;

    }

}
